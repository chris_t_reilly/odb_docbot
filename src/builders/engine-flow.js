var Promise = require("promise");
var path = require("path");
var os = require("os");
var log = require('npmlog');
var http = require('http');
var pace = require('pace');
var fs = require('fs');
var Decompress = require('decompress');
var zip = require('decompress-unzip');
var tar = require('tar-fs');
var markdown = require( "markdown" ).markdown;

log.level = "verbose";
var build = function(config, logLevel) {
	log.level = logLevel;
	return new Promise(function(resolve, reject) {

		
		var repository = "ssh://"+config.git.host+"/"+config.git.repository;
		var destination = path.join(os.tmpdir(), config.git.repository.split("/")[1].replace('.git', ''));
		log.info('engine-flow', 'Fetching the tag '+config.git.tag+' on repo '+repository);

		require('child_process').exec('git archive --format=tar --remote='+repository+' --output="'+destination+'-'+config.git.tag+'.tar" '+config.git.tag, function(error, stdout, stderr) {

			if(error) {
				reject(error);
			} else {
				log.info('engine-flow', 'Unpacking tarball ('+destination+'-'+config.git.tag+'.tar -> '+destination+')');
				var write = tar.extract(destination+'-'+config.git.tag);
				var read = fs.createReadStream(destination+'-'+config.git.tag+'.tar').pipe(write);
				
				write.on('finish', function() {
					log.info('engine-flow', 'Installing...');
					require('child_process').exec('cd '+destination+'-'+config.git.tag+' && mvn clean install ', function(error, stdout, stderr) {
						if(error) {
							reject(error);
						} else {
							var mcTests = [];
							
							mcTests.push(runMonteCarloTest(destination+'-'+config.git.tag, config.serial, config.monteCarlo.iterations, config.monteCarlo.runs));
							
							Promise.all(mcTests).then(function(testResults) {
								var pages = [];
								var navTree = {};

								for(var i = 0; i < testResults.length; i++) {
									for(var j = 0; j < testResults[i].flows.length; j++) {
										var splitAction = testResults[i].flows[j].action.split("/");
										testResults[i].flows[j].action = splitAction[splitAction.length-1];
									}
									
									testResults[i].actions.sort();
									testResults[i].events.sort();
									testResults[i].contexts.sort();
									testResults[i].availableActions.sort();
									
									pages.push({
										title: testResults[i].model.Name,
										fileName: path.join("engine-flow", testResults[i].model.Name+".html"),
										version: config.git.tag,
										template: "engineFlow",
										gameData: testResults[i]
									});

									navTree[testResults[i].model.Name] = {
										href: "engine-flow/"+encodeURIComponent(testResults[i].model.Name)+".html",
										children: []
									};
								}
								
								log.info("engine-flow", "Finished");
								resolve({
									module: "engine-flow",
									pages: pages,
									title: "Engine flow",
									version: config.git.tag,
									navTree: {
										"Engine flow": {
											href: pages[0].fileName,
											children: navTree
										}
									}
								});	
							}).catch(reject);
						}
					});
				});
				
				write.on('error', function(error){
					log.error(error);
					reject(error);
				});
			}
		});		
	});			
};

var runMonteCarloTest = function(directory, serial, iterations, runs) {
	return new Promise(function(resolve, reject) {
		log.info('engine-flow', 'Running Monte Carlo tests for '+serial);
		log.verbose('engine-flow', 'cd '+directory+' && mvn -f server/pom.xml exec:java -Dexec.mainClass="com.odobo.games.rapidslot.montecarlo.MonteCarloCheckerForAllSerials" -Dexec.classpathScope="test" -Dexec.args="'+iterations+' '+runs+' '+serial+'"');
		require('child_process').exec('cd '+directory+' && mvn -f server/pom.xml exec:java -Dexec.mainClass="com.odobo.games.rapidslot.montecarlo.MonteCarloCheckerForAllSerials" -Dexec.classpathScope="test" -Dexec.args="'+iterations+' '+runs+' '+serial+'"', function(error, stdout, stderr) {
			if(error) {
				reject(error)
				console.log(stderr, stdout);
			} else {				
				log.info('engine-flow', 'Processing Monte Carlo results for '+serial);
				fs.readFile(path.join(directory, 'mc_data_'+serial+'.json'), 'utf8', function(error, file) {
					if(error) {
						reject(error);
					} else {
						resolve(JSON.parse(file));
					}
				});
			}
		});
	});
};

module.exports = build;