var Promise = require('promise');
var utils = require('../utils');
var fs = require('fs');
var os = require('os');
var Download = require('download-http');
var http = require('http');
var path = require('path');
var globby = require('globby');
var xmlParser = require('xml2json');
var Decompress = require('decompress');
var zip = require('decompress-unzip');
var rest = require('restler');
var pace = require('pace');
var log = require('npmlog');
var ProgressBar = require('progress');
var markdown = require( "markdown" ).markdown;
var config, repository;

var build = function(config) {

	return new Promise(function(resolve, reject) {
		
		var game = null;
		
		var xsdRequests = [];
		log.info('engine-protocol', 'Downloading JAR File', 'Started');
		
		xmlToJSON(path.join(getUserHome(), '.m2/settings.xml')).then(function(mavenConfig){
			
			return jarDownload(config, getMavenServer(mavenConfig, config.nexus.server));
		})
		.then( function( g ) {
			log.info('engine-protocol', 'Installing JAR');
			game = g;
			return installDependencies(path.join(game, "META-INF", "maven", "com.odobo.games."+config.nexus.name, config.nexus.name+"-game"));
		})
		.then( openDependencyJars )
		.then( function(dependencyPaths) {
			var xsdChecks = [];
			var pomChecks = [];
			log.info('engine-protocol', 'Checking for XSD Files. This might take a while.');
			for(var i = 0; i < dependencyPaths.length; i++) {
				xsdChecks.push(checkDirForFiles(dependencyPaths[i], '**.xsd'));
			}

			xsdChecks.push(checkDirForFiles(game, '**.xsd'));

			Promise.all(xsdChecks).then(function(xsdFiles) {

				xsdFiles = xsdFiles.filter(function(n){ return n != undefined });
				
				xsdFiles = xsdFiles.filter(function(n){ 
					return config.nexus.ignoreArtifacts.indexOf(n.pom.project.artifactId) < 0 
				});


				var xsdConversion = [];
				
				
				for(var i = 0; i < xsdFiles.length; i++) {
					for (var j = 0; j < xsdFiles[i].paths.length; j++) {
						xsdConversion.push(parseXSD(path.join(xsdFiles[i].dir, xsdFiles[i].paths[j]), xsdFiles[i].pom));
						
					}
				}

				log.info('engine-protocol', 'Converting XSDs to JSON');
				Promise.all(xsdConversion).then(function(convertedXSDs) {
					var pages = [];
					var links = {};
					var namespaces = {};

					for(var i = 0; i < convertedXSDs.length; i++) {
						var component = convertedXSDs[i].xsd['xs:schema'];
						for(var key in component) {

							if(key.indexOf('xmlns:') > -1) {
								
								var link = component[key].replace('http://games.odobo.com/', '').replace('http://gamecomponents.odobo.com/', '')+".html";
								namespaces[key.replace('xmlns:', '')] = encodeURIComponent(link);
							}
						}
						var componentName = component.targetNamespace.replace('http://games.odobo.com/', '');
						componentName = componentName.replace('http://gamecomponents.odobo.com/', '');
						
						links[componentName] = {
							href: "engine-protocol/"+encodeURIComponent(componentName)+'.html',
							children: {}
						};

						if(convertedXSDs[i].xsd['xs:schema']['xs:element']) {
							convertedXSDs[i].xsd['xs:schema']['xs:element'].sort(function(a, b) {
								if (a.name > b.name) {
									return 1;
								}
								if (a.name < b.name) {
									return -1;
								}
								return 0;
							});
						}
						
						if(Array.isArray(convertedXSDs[i].xsd['xs:schema']['xs:complexType'])) {
							convertedXSDs[i].xsd['xs:schema']['xs:complexType'].sort(function(a, b) {
								if (a.name > b.name) {
									return 1;
								}
								if (a.name < b.name) {
									return -1;
								}
								return 0;
							});
						}

						pages.push({
							title: componentName,
							fileName: path.join("engine-protocol", componentName+'.html'),
							version: convertedXSDs[i].pom.project.parent.version || config.nexus.version,
							pom: convertedXSDs[i].pom,
							template: "engineProtocol",
							data: convertedXSDs[i].xsd,
							namespaces: namespaces
						});
					}

					var keys = [];

					for(var componentName in links) {
						if(links.hasOwnProperty(componentName)) {
							keys.push(componentName);
						}
					}
					var sortedComponentNames = {};
					keys.sort();
					for(var i = 0; i < keys.length; i++) {
						sortedComponentNames[keys[i]] = links[keys[i]];
					}
					
					log.info('engine-protocol', 'Finished')
					resolve({
						module: "engine-protocol",
						pages: pages,
						title: "Engine protocol",
						navTree: {
							"Engine protocol": {
								href: pages[0].fileName,
								children: sortedComponentNames
							}
						}
					});
				}).catch(reject);
			}).catch(reject);			
		}).catch(reject);
	});
};

var getMavenServer = function(mavenSettings, serverID) {
	
	if(Array.isArray(mavenSettings.settings.servers.server)) {
		console.log("isArray", mavenSettings.settings.servers.server.length);
		for(var i = 0; i < mavenSettings.settings.servers.server.length; i++) {
			console.log(serverID);
			if(mavenSettings.settings.servers.server[i].id === serverID) {

				return mavenSettings.settings.servers.server[i];
			}
		}
	} else {
		console.log("NOT AN ARRAY", JSON.stringify(mavenSettings.settings.servers));
		if(mavenSettings.settings.servers.server.id === serverID) {
			return mavenSettings.settings.servers.server;
		}
	}

	throw new Error("No maven credentials found for server "+serverID);
};

var getUserHome = function() {
  return process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'];
}

var parseXSD = function(path, pom) {
	return new Promise(function(resolve, reject) {
		var output;
		fs.readFile(path, 'utf8', function(error, file) {
			if(error) {
				reject(error);
			} else {

				resolve({xsd: JSON.parse(xmlParser.toJson(file)), pom: pom});
			}
		});
	});
};

var openDependencyJars = function(baseDir) {
	log.info('engine-protocol', 'Opening Dependency Jars');
	return new Promise(function(resolve, reject) {

		globby("**.jar", {cwd: baseDir}, function(error, files) {
			if(error) {
				reject(error);
			} else {
				var jars = [];
				for(var i = 0; i < files.length; i++) {
					jars.push(openJar(path.join(baseDir, files[i])));
				}
				Promise.all(jars).then(resolve, reject);
			}
		});
	});
};

var installDependencies = function(pomDir) {
	return new Promise(function(resolve, reject) {
		log.info('engine-protocol', 'Installing JAR Dependencies');
		var exec = require('child_process').exec;
		exec('cd '+pomDir+' && ls && mvn dependency:copy-dependencies -DaddParentPoms && cd -', function (error, stdout, stderr) {
			if(error) {
				reject(error);
			}
			log.info(stderr);
			resolve(path.join(pomDir, 'target/dependency'));
			log.info('engine-protocol', 'Installing JAR', 'Complete');
		});
	});
};

var checkDirForFiles = function(dir, fileType) {
	return new Promise(function(resolve, reject) {
		log.info('engine-protocol', 'Checking For Files', 'Checking for '+fileType+' files in '+dir);
		globby(fileType, {cwd: dir}, function(error, xsdFiles) {
			if(error) {
				reject(error);
			} else {
				globby("**/**/pom.xml", {cwd: dir}, function(error, pomFile) {
					if(error) {
						reject(error);
					} else {
						if(xsdFiles.length > 0) {
							resolve({dir: dir, paths: xsdFiles, pom: JSON.parse(xmlParser.toJson(fs.readFileSync(path.join(dir, pomFile[0]))))});
						} else {
							resolve(undefined);
						}
					}
				});
				
			}
		});
	});
};

var xmlToJSONDownload = function(url) {
	return new Promise(function(resolve, reject) {
		var req = http.get(url, function(res) {
			// save the data
			var xml = '';
			res.on('data', function(chunk) {
			xml += chunk;
			});

			res.on('end', function() {
				resolve(JSON.parse(xmlParser.toJson(xml)));
			});
		});

		req.on('error', function(error) {
			reject(error);
		});
	});
};

var xmlToJSON = function(path) {
	return new Promise(function(resolve, reject) {
		fs.readFile(path, function(error, file) {
			if(error) {
				reject(error);
			} else {
				resolve(JSON.parse(xmlParser.toJson(file)));
			}
			
		});		
	});
};

var pomDownload = function(url) {
	return new Promise(function(resolve, reject) {
		var localPath = path.join(os.tmpdir(), path.join(path.basename(url).slice(0, -4), path.basename(url)));
		Download(url, localPath, function(error) {
			if(error) {
				reject(error);
			} else {
				resolve(localPath);
			}
		});
	});
}

var openJar = function(jarPath, bar) {
	
	return new Promise(function(resolve, reject) {

		require('child_process').exec('cd '+path.dirname(jarPath)+'; mkdir '+jarPath.slice(0, -4)+'; cd '+jarPath.slice(0, -4)+'; jar xf '+jarPath, function(error, stdout, stderr) {
			if(error) {
				reject(error);
			} else {
				resolve(jarPath.slice(0, -4));
			}
		});
	});
};

var jarDownload = function(config, mavenConfig) {
	return new Promise(function(resolve, reject) {
		var localPath = path.join(os.tmpdir(), config.nexus.name+config.nexus.version+".jar");
		var urlPath = "/nexus/service/local/repositories/"+config.nexus.repository+"/content/com/odobo/games/"+config.nexus.name+"/"+config.nexus.name+"-game/"+config.nexus.version+"/"+config.nexus.name+"-game-"+config.nexus.version+".jar";
		var options = {
			host: config.nexus.host,
			port: config.nexus.port,
			path: urlPath,
			method: "GET",
			auth: mavenConfig.username+":"+mavenConfig.password
		};

		log.info('engine-protocol', 'Downloading JAR File', "URL", "http://"+config.nexus.host+":"+config.nexus.port+urlPath);

		var request = http.request(options, function(response) {
			if(response.statusCode !== 200) {
				var error = new Error("Couldn't download "+config.nexus.name+": "+response.statusCode);
				log.error('Downloading JAR File', 'Error', error);
				reject(error);
			}

			var len = parseInt(response.headers['content-length'], 10);
			var bar = pace(len);
			var complete = 0;
			response.on('data', function(chunk) {
				complete += chunk.length;
				bar.op(complete);
				fs.appendFile(localPath, chunk, function(error) {
					if(error) {
						reject(error);
					}
				});
			});

			response.on('end', function() {
				if(!error) {

					log.info('engine-protocol', 'Downloading JAR File', 'Complete:'+localPath);
					openJar(localPath).then(resolve).catch(reject);	
				}
			});
			
		});

		request.on('error', function(error) {
			reject(error);
		});

		request.end();
	});
};

module.exports = build;