#! /usr/bin/env node

var fs = require('fs');
var fse = require('fs-extra');
var path = require('path');
var Promise = require('promise');
var utils = require('./utils');
var swig = require('swig');
var log = require('npmlog');
var appDir = __dirname;

swig.setFilter('hasOpenChild', function(element, children, pageTitle) {
	return hasOpenChild(element, children, pageTitle);
});

swig.setFilter('type', function(element) {
	if(Array.isArray(element)) {
		return 'Array';
	} else if(typeof element === "object") {

		return 'Object';
	} else {
		return 'Primitive'
	}
});

function hasOpenChild(elementName, children, pageTitle) {
	var result;
	if(elementName === pageTitle) {
		return true;
	}
	if(children) {
		for(var child in children) {

			result = hasOpenChild(child, children[child].children, pageTitle);

			if(result != false) {
				return result;
			}
		}
		return false;
	} else {
		return false;
	}
}

var pageTemplate = swig.compileFile(path.join(appDir, 'frontend/index.html'), {autoescape: false});
var message = fs.readFileSync(path.join(__dirname, './message.txt'), 'utf8');
var pageBuildPromises = [];
utils.jsonFileToObject('docbot.json').then(function(config) {
	var builders = [];
	console.log(message);
	log.info("doc-bot", "Starting up");
	for(var builder in config.builders) {
		if(!config.builders[builder].skip) {
			log.info("doc-bot", 'Started build process ./builders/'+builder)
			var builderProcess = require('./builders/'+builder)(config.builders[builder], config.logLevel);
			
			builders.push(builderProcess)
		} else {
			log.warn("doc-bot", "Skipping builder ./builders/"+builder);
		}
	}

	Promise.all(builders).then(function(builderData){


		fse.mkdirs('documentation/build', function(error) {
			if(error) {
				console.error(error);
				throw error;
			} else {
				fse.copy(path.join(__dirname, 'frontend/bower_components'), 'documentation/build/bower_components', function(error) {
					fse.copy(path.join(__dirname, 'frontend/scripts'), 'documentation/build/scripts', function(error) {
						fse.copy(path.join(__dirname, 'frontend/styles'), 'documentation/build/styles', function(error) {
							if(error) {
								console.error(error);
								throw error;
							} else {
								var navigation = {};
								for(var i = 0; i < builderData.length; i++) {
									for(var navEntry in builderData[i].navTree) {
										navigation[navEntry] = builderData[i].navTree[navEntry];
									}
								}
								for(var i = 0; i < builderData.length; i++) {
									if(builderData[i]) {
										for(var j = 0; j < builderData[i].pages.length; j++) {
											pageBuildPromises.push(buildPage({
												year: new Date().getFullYear(),
												navigation: navigation,
												page: builderData[i].pages[j]
											}));
										}
									}
								}
								Promise.all(pageBuildPromises).then(function(){
									log.info("doc-bot", "Shutting down");
								}).catch(function(exception){
									log.error("doc-bot", exception);
								});
							}
						});
					});
				});
			}
		});
	}).catch(function(exception){
		log.error("doc-bot", exception);
		log.error("doc-bot", exception.stack);
		process.exit(1);
	});
}).catch(function(exception){
	log.error("doc-bot", exception);
	log.error("doc-bot", exception.stack);
	process.exit(1);
});


var buildPage = function(data) {
	return new Promise(function(resolve, reject) {
		log.info("doc-bot", "Building page "+data.page.fileName);
		var p = path.join('documentation', 'build', data.page.fileName);
		fse.mkdirs(path.dirname(p), function(error) {
			if(error) {
				reject(error);
				console.log(error);
			} else {
				resolve();
				fs.writeFile(p, pageTemplate(data), function(error){
					if(error) {
						reject(error);
						console.log(error);
					} else {
						resolve();
					}
				});
			}
		});
	});
};