var Promise = require('promise');
var fs = require('fs');

var jsonFileToObject = function(path) {
	return new Promise(function(resolve, reject) {
		fs.readFile(path, 'utf8', function(error, file) {
			if(error) {
				reject(error);			
			} else {
				resolve(JSON.parse(file));
			}
		});
	});
}

var filePromise = function(path) {

	return new Promise(function(resolve, reject) {
		fs.readFile(path, 'utf8', function(error, file) {
			if(error) {
				reject(error);			
			} else {
				resolve({file: file, path: path});
			}
		});
	});
}

var cloneObject = function(clone) {
	return JSON.parse(JSON.stringify(clone));
};

module.exports = {
	jsonFileToObject: jsonFileToObject,
	filePromise: filePromise,
	cloneObject: cloneObject
}